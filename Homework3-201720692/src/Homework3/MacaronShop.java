package Homework3;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MacaronShop {
    private int indexNextCustomer = 0;
    private int sales = 0;
    private final int[] orderList;
    private final int price;

    public MacaronShop(int price, int[] orderList) {
        this.price = price;
        this.orderList = orderList;
    }

    public void openShop(final int CASHERS){

        ExecutorService executorService = Executors.newFixedThreadPool(CASHERS);
        for(int i = 0; i < CASHERS; i++) {
            Casher casher = new Casher(this);
            executorService.execute(casher);
        }
        executorService.shutdown();
    }

    public int getIndexNextCustomer() {
        return indexNextCustomer;
    }

    public void setIndexNextCustomer(int indexNextCustomer) {
        this.indexNextCustomer = indexNextCustomer;
    }

    public int getSales() {
        return sales;
    }

    public void setSales(int sales) {
        this.sales = sales;
    }

    public int[] getOrderList() {
        return orderList;
    }

    public int getPrice() {
        return price;
    }

}
