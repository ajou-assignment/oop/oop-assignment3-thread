package Homework3;

import java.util.Random;
import java.util.Scanner;
import java.util.stream.IntStream;

public class TestDriver {

    public static int getRandomInt(int min, int max) {
        Random random = new Random();
        return random.nextInt(max - min + 1) + min;
    }

    public static void main(String[] args) {
        final int CASHERS = 3;
        int ORDERS;
        int PRICE;
        final Scanner scanner = new Scanner(System.in);
        final MacaronShop ms;

        final int ORDER_MIN = 1;
        final int ORDER_MAX = 10;
        final int CUSTOMER_MIN = 5;
        final int CUSTOMER_MAX = 15;

        System.out.printf("Please enter the price of the macaron >> ");
        PRICE = scanner.nextInt(); scanner.nextLine();
        ORDERS = getRandomInt(CUSTOMER_MIN, CUSTOMER_MAX);

        int[] orderList = new int[ORDERS];
        IntStream.range(0, ORDERS).forEach(i-> orderList[i] = getRandomInt(ORDER_MIN, ORDER_MAX));

        ms = new MacaronShop(PRICE, orderList);
        ms.openShop(CASHERS);
    }
}
