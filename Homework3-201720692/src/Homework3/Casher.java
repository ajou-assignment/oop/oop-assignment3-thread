package Homework3;

public class Casher implements Runnable{
    private final MacaronShop myShop;
    private final int PACK_TIME =  300;

    public Casher(MacaronShop ms) {
        myShop = ms;
    }


    private void make() {
        try {
            Thread.sleep(PACK_TIME);
        }
        catch(InterruptedException e) {
            e.printStackTrace();
        }
    }

    private int pack(int price, int macaronNum) {
        int sales = 0;

        for(int i = 0;i < macaronNum; i++){
            // instead of sleep(300 * macaronNum);
            make();
            sales += price;
        }

        return sales;
    }
     public void run() {
        {
            int price = myShop.getPrice();
            int[] orderList = myShop.getOrderList();

            int orderIndex;
            do {
                int order;
                synchronized (myShop) {
                    orderIndex = myShop.getIndexNextCustomer();
                    myShop.setIndexNextCustomer(orderIndex + 1);
                    order = orderList[orderIndex];
                    System.out.printf("Packing %d macarons...\n", order);
                }

                int sales = pack(price, order);

                synchronized (myShop) {
                    myShop.setSales(myShop.getSales() + sales);
                    System.out.printf("Sold %d macarons and earned $%d ! Macaron shop earned %d today.\n", order, sales, myShop.getSales());
                }

            } while (myShop.getIndexNextCustomer() < orderList.length);
        }
    }
}
